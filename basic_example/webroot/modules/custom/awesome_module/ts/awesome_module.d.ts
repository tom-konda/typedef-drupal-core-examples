import { ajaxCommand, commandDefinition } from "@tom-konda/typedef-drupal-core/drupal.ajax"

declare global {
  namespace Drupal {
    interface definedBehaviorWithCustomProps {
      example: {
        awesomeSettings: {
          require_string: string,
          optional_number?: number,
        }
      }
    }

    namespace theme {
      let exampleTheme : (string: string) => string
    }

    namespace Ajax {
      interface definedCommands {
        example_command: ajaxCommand<
          'example_command',
          Pick<commandDefinition, 'selector'> & {
            threshold: number,
          },
          false
        >
      }
    }
  }
}
