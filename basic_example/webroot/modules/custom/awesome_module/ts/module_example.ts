
(
  (Drupal, $) => {
    Drupal.behaviors.example = {
      awesomeSettings: {
        require_string: 'example',
      },
      attach(context) {
        const target = context.querySelector('.some-class');
        once('once-example', target, context).forEach(
          element => {
            console.log(element);
            const thresholdNumber = this.awesomeSettings.optional_number ?? -1;
            if (thresholdNumber > 0) {
              console.log(`Optional number is ${thresholdNumber}`);
            }
          }
        );

        const $target = $(context).find('.some-class');
        $target.once('old-once-example')
          .on(
            'click',
            function () {
              $(this).toggle();
              const message = new Drupal.Message();
              message.add(Drupal.theme('exampleTheme', 'Awesome'));
            }
          )
      }
    }

    Drupal.AjaxCommands.prototype.example_command = (ajax, response, status) => {
      if (response.threshold <= 0) {
        return false;
      }
      console.log(ajax.element_settings, `HTTP status ${status}`);
    }

    Drupal.theme.exampleTheme = (string) => {
      return `<div class="example">${Drupal.checkPlain(string)}</div>`
    }
  }
)(Drupal, jQuery)